 Eclipse Project to test OpenCv and Tesseract with Windows

* default configuration for x86_x64, but support included for x86

* Import this project in Eclipse.

* Run class TestOpencvJava as a java Application.

* All images in folder OCR_IMG will be processed.

![project_build_path.png](https://bitbucket.org/repo/RyLdRL/images/1575379931-project_build_path.png)*

References

* [Tess4J](http://tess4j.sourceforge.net/)

* [OpenCv](http://opencv.org/)