package es.rburon.test;

import java.io.File;
import java.util.List;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;

import es.rburon.img.ImgHandler;

public class TestOpencv {

	public static void main(String[] args) {
		System.setProperty("jna.library.path", "32".equals(System
				.getProperty("sun.arch.data.model")) ? "lib/win32-x86"
				: "lib/win32-x86-64");
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		File image = null;
		File imageFolder = null;
		Mat matImage;
		// Get the Folder with the images and list them
		imageFolder = new File("OCR_IMG");
		File[] images = imageFolder.listFiles();
		// Configure Tesseract with english french and dutch
		Tesseract instance = new Tesseract();
		instance.setDatapath("tesseract/tessdata");
		instance.setLanguage("eng+nld+fra");
		for (int i = 0; i < images.length; i++) {
			image = images[i];
			if (image.isFile()) {
				System.out.println("Reading Image: " + image.getName());
				// Load the image into a Mat
				matImage = Highgui.imread(image.getAbsolutePath());
				if (!matImage.empty()) {
					DisplayImage displayImg = new DisplayImage();
					System.out.println("Improving Image: " + image.getName());
					ImgHandler imgHandler = new ImgHandler();
					// Get the Mat with the big square, forms with no space
					// outside in the picture will not be recognized cause they
					// are not really squares, this test is not ready to think
					// that picture is the form itself we try to detect the form
					// in a camera picture or an scanned image
					Mat imgEdge = imgHandler.getSquareMat(matImage);
					if (imgEdge == null)
						continue;
					// Perform the HoughLines operation inside the frame, this
					// will help to detect the angle of the camera focus to try
					// to correct the image, this is not implemented yet
					Mat imgGray = imgHandler.colorToGray(imgEdge);
					Mat imgCanny = imgHandler.cannyThreshold(imgGray);
					List<Point[]> houghLines = imgHandler
							.getHoughLines(imgCanny);
					imgHandler.drawHoughLines(matImage, houghLines);

					// prepare the images to resize and fit in the display and
					// show the tests
					Size originalSize = matImage.size();
					int[] DestinationSizes = displayImg.getImgSize(
							originalSize.width, originalSize.height);

					Mat resizeImg = imgHandler.resize(matImage,
							DestinationSizes[0], DestinationSizes[1]);
					Size resizedSize = resizeImg.size();
					Mat resizeImgEdge = imgHandler.resize(imgEdge,
							DestinationSizes[0], DestinationSizes[1]);
					System.out.println("Original :" + originalSize.width + "x"
							+ originalSize.height + " Resized : "
							+ resizedSize.width + "x" + resizedSize.height);
					displayImg
							.showTop(imgHandler.matToBufferedImage(resizeImg));
					displayImg.showBot(imgHandler
							.matToBufferedImage(resizeImgEdge));
					displayImg.setVisible(true);

					String result = null;
					System.out.println("Starting OCR on Image: "
							+ image.getName());
					try {
						result = instance.doOCR(imgHandler
								.prepareToOcr(imgGray));
						System.out.println("Processed Image: "
								+ image.getName() + " with result of: "
								+ result);
					} catch (TesseractException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					System.out.println("Reading Error: Image "
							+ image.getName()
							+ "is not valid process with opencv");
				}
			}
		}
	}
}
