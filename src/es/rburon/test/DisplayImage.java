package es.rburon.test;
/**
 * 
 */
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.FlowLayout;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
/**
 * @author buronix
 *
 */
public class DisplayImage {
	
	ImageIcon icon = null;
	JLabel topLbl = new JLabel();
	JLabel botLbl = new JLabel();
	JFrame mainFrame = null;
	Dimension screenDimension = null;
	private Double SCREEN_HEIGHT;
	private Double SCREEN_WIDTH;
	private int refactorScreenHeight;
	private int refactorScreenWidth;
	private int imageHeight;
	private int imageWidth;
	private static double refactor = 0.95;
	
	public DisplayImage()
    {
		screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
		SCREEN_HEIGHT = (Double)screenDimension.getHeight();
		SCREEN_WIDTH = (Double)screenDimension.getWidth();
		mainFrame=new JFrame();
		mainFrame.setSize(SCREEN_WIDTH.intValue(), SCREEN_HEIGHT.intValue());
		refactorScreenHeight = (int)Math.round(SCREEN_HEIGHT.intValue()*refactor);
		refactorScreenWidth = (int)Math.round(SCREEN_WIDTH.intValue()*refactor);
		imageWidth = refactorScreenWidth;
		imageHeight = (int)Math.round(refactorScreenHeight/2);
		mainFrame.setLayout(new FlowLayout());
		topLbl.setSize(imageWidth, imageHeight);
		botLbl.setSize(imageWidth, imageHeight);
        mainFrame.add(topLbl);
        mainFrame.add(botLbl);
    }
	
	public int[] getImgSize(double originalWidth, double originalHeight ){
		
		double refactorWidth = 0;
		double refactorHeight = 0;
		int newImageWidth = 0;
		int newImageHeight = 0;
		refactorHeight = imageHeight/originalHeight;
		if(originalWidth*refactorHeight>imageWidth){
			refactorWidth = imageWidth/(originalWidth*refactorHeight);
		}else{
			refactorWidth = 1;
		}
		newImageWidth = (int)Math.round(originalWidth * refactorHeight * refactorWidth);
		newImageHeight = (int)Math.round(originalHeight * refactorHeight * refactorWidth);
		int[] sizes = {newImageWidth, newImageHeight};
		return sizes;
	}
	
	public void showTop(BufferedImage img){
		icon = new ImageIcon(img);
		topLbl.setIcon(icon);
	}
	
	public void showBot(BufferedImage img){
		icon = new ImageIcon(img);
		botLbl.setIcon(icon);
	}
	public void setVisible(boolean visible){
        mainFrame.setVisible(true);
	}
}
