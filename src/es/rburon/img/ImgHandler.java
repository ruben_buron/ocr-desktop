/**
 * 
 */
package es.rburon.img;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 * @author buronix
 *
 */
public class ImgHandler {

	private Size kernerlSize = new Size(3, 3);
	private static int APERTURE_SIZE = 5;
	private static double LOWER_THRESHOLD = 45;
	private static double UPPER_THRESHOLD = 75;
	private static Scalar COLOR_GREEN = new Scalar(0, 255, 0);

	/**
	 * 
	 */
	public ImgHandler() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * resize an image to a custom width and height
	 * 
	 * @param originalImg
	 *            the image to resize
	 * @param width
	 *            the new with of the output image
	 * @param height
	 *            the new height of the output image
	 * @return the Mat container for the resized image
	 */
	public Mat resize(Mat originalImg, int width, int height) {
		Mat resizedImg = new Mat();
		Size newSize = new Size(width, height);
		Imgproc.resize(originalImg, resizedImg, newSize);
		return resizedImg;
	}

	/**
	 * Convert from a Mat image matrix to an BurfferedImage Java object
	 * 
	 * @param matrix
	 * @return
	 */
	public BufferedImage matToBufferedImage(Mat matrix) {
		int cols = matrix.cols();
		int rows = matrix.rows();
		int elemSize = (int) matrix.elemSize();
		byte[] data = new byte[cols * rows * elemSize];
		int type;

		matrix.get(0, 0, data);

		switch (matrix.channels()) {
		case 1:
			type = BufferedImage.TYPE_BYTE_GRAY;
			break;

		case 3:
			type = BufferedImage.TYPE_3BYTE_BGR;

			// bgr to rgb
			byte b;
			for (int i = 0; i < data.length; i = i + 3) {
				b = data[i];
				data[i] = data[i + 2];
				data[i + 2] = b;
			}
			break;

		default:
			return null;
		}

		BufferedImage image = new BufferedImage(cols, rows, type);
		image.getRaster().setDataElements(0, 0, cols, rows, data);

		return image;
	}

	/**
	 * perform a cannyThreshold operation by default of a orignal gray Mat
	 * 
	 * @param imgGray
	 *            mat to perform canny operation
	 * @return canny Mat
	 */
	public Mat cannyThreshold(Mat imgGray) {
		Mat cannyImg = new Mat();
		cannyImg.create(imgGray.size(), imgGray.type());
		// / Reduce noise with a kernel 3x3
		Imgproc.GaussianBlur(imgGray, cannyImg, kernerlSize, APERTURE_SIZE,
				APERTURE_SIZE);
		// / Canny detector
		Imgproc.Canny(cannyImg, cannyImg, LOWER_THRESHOLD, UPPER_THRESHOLD);
		return cannyImg;
	}

	/**
	 * Method to draw lines into a Mat from a List of points
	 * 
	 * @param rgbaMat
	 *            Mat to draw the lines
	 * @param houghLines
	 *            List of points to draw the lines
	 */
	public void drawHoughLines(Mat rgbaMat, List<Point[]> houghLines) {
		for (Point[] line : houghLines) {
			Core.line(rgbaMat, line[0], line[1], COLOR_GREEN, 1);
		}
	}

	/**
	 * calculate the angle of three Points
	 * 
	 * @param pt1
	 *            first point
	 * @param pt2
	 *            second point
	 * @param pt0
	 *            third point
	 * @return the double angle value
	 */
	public double angle(Point pt1, Point pt2, Point pt0) {
		double dx1 = pt1.x - pt0.x;
		double dy1 = pt1.y - pt0.y;
		double dx2 = pt2.x - pt0.x;
		double dy2 = pt2.y - pt0.y;
		return (dx1 * dx2 + dy1 * dy2)
				/ Math.sqrt((dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2)
						+ 1e-10);
	}

	/**
	 * Perform a conversion from a color image to gray scale
	 * 
	 * @param original
	 *            image to convert
	 * @return the gray Mat image
	 */
	public Mat colorToGray(Mat original) {
		Mat gray = new Mat(original.size(), original.type());
		Imgproc.cvtColor(original, gray, Imgproc.COLOR_RGB2GRAY);
		return gray;
	}

	/**
	 * return a mat with the biggest square frame detected on it, color
	 * detection included
	 * 
	 * @param image
	 *            image to detect the contours
	 * @return the reframed Mat of the biggest frame from the original Mat image
	 */
	public Mat getSquareMat(Mat image) {
		Mat blurred = new Mat(image.size(), image.type());
		Mat dst = Mat.zeros(image.size(), image.type());
		Mat gray0 = this.colorToGray(image);
		Imgproc.medianBlur(image, blurred, 5);
		Imgproc.cvtColor(image, gray0, Imgproc.COLOR_RGB2GRAY);
		Mat gray = new Mat();
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		List<MatOfPoint> squares = new ArrayList<MatOfPoint>();

		for (int c = 0; c < 3; c++) {
			int ch[] = { c, 0 };
			MatOfInt fromto = new MatOfInt(ch);
			List<Mat> blurredlist = new ArrayList<Mat>();
			List<Mat> graylist = new ArrayList<Mat>();
			blurredlist.add(0, blurred);
			graylist.add(0, gray0);
			Core.mixChannels(blurredlist, graylist, fromto);
			gray0 = graylist.get(0);
			int threshold_level = 3;
			for (int l = 1; l <= threshold_level; l++) {
				Imgproc.Canny(gray0, gray, 20 * l, 15 * 2 * l);
				Imgproc.dilate(gray, gray, Mat.ones(new Size(3, 3), 0));
				Imgproc.findContours(gray, contours, new Mat(), 1, 2);
				MatOfPoint2f approx = new MatOfPoint2f();
				MatOfPoint2f mMOP2f1 = new MatOfPoint2f();
				MatOfPoint mMOP = new MatOfPoint();
				for (int i = 0; i < contours.size(); i++) {
					contours.get(i).convertTo(mMOP2f1, CvType.CV_32FC2);
					Imgproc.approxPolyDP(mMOP2f1, approx,
							Imgproc.arcLength(mMOP2f1, true) * 0.02, true);
					approx.convertTo(mMOP, CvType.CV_32S);

					if (approx.rows() == 4
							&& Math.abs(Imgproc.contourArea(approx)) > 1000
							&& Imgproc.isContourConvex(mMOP)) {
						double maxcosine = 0;
						Point[] list = approx.toArray();
						for (int j = 2; j < 5; j++) {
							double cosine = Math.abs(angle(list[j % 4],
									list[j - 2], list[j - 1]));
							maxcosine = Math.max(maxcosine, cosine);
						}
						if (maxcosine < 0.3) {
							MatOfPoint temp = new MatOfPoint();
							approx.convertTo(temp, CvType.CV_32S);
							squares.add(temp);
						}
					}

				}
			}
		}

		double maxarea = 0;
		double secmaxarea = 0;
		int maxareaidx = 0;
		for (int idx = 0; idx < squares.size(); ++idx) {
			Mat contour = squares.get(idx);
			double area = Math.abs(Imgproc.contourArea(contour));
			if (area > maxarea) {
				maxarea = area;
				maxareaidx = idx;
			}
		}

		for (int idx = 0; idx < squares.size(); ++idx) {
			Mat contour = squares.get(idx);
			double area = Imgproc.contourArea(contour);
			if (area > secmaxarea && area < maxarea) {
				secmaxarea = area;
			}
		}
		if (!(squares.isEmpty())) {
			Mat mask = Mat.zeros(dst.size(), dst.type());
			Scalar s = new Scalar(255, 255, 255);
			Imgproc.drawContours(mask, squares, maxareaidx, s, -1);
			Mat canvas = new Mat(image.size(), image.type());
			Scalar s2 = new Scalar(0, 0, 0);
			canvas.setTo(s2);
			image.copyTo(canvas, mask);
			return canvas;
		} else {
			return null;
		}
	}

	/**
	 * this was the first function to perform contour and shape detection, draw
	 * into the original image the biggest square and return the original image
	 * with the frame
	 * 
	 * @param image
	 *            image to perform the test operation
	 * @return the image with the biggest square in blue
	 */
	public Mat edgeDetection(Mat image) {
		Mat blurred = new Mat();
		Mat dst = Mat.zeros(image.size(), image.type());
		image.copyTo(blurred);
		System.out.println("Blurred Matrix! : " + blurred.total());
		// Imgproc.GaussianBlur(image, blurred, kernerlSize, 2, 2);
		Imgproc.medianBlur(image, blurred, 5);
		System.out.println("Median Blur Done!");

		Mat gray0 = new Mat(blurred.size(), blurred.type());
		Imgproc.cvtColor(gray0, gray0, Imgproc.COLOR_RGB2GRAY);
		Mat gray = new Mat();

		System.out.println("Gray0 Matrix! : " + gray0.total());
		System.out.println("Gray Matrix! : " + gray.total());

		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		List<MatOfPoint> squares = new ArrayList<MatOfPoint>();

		// find squares in every color plane of the image
		for (int c = 0; c < 3; c++) {
			// System.out.println("Mix Channels Started! : " + gray0.total());
			int ch[] = { c, 0 };
			MatOfInt fromto = new MatOfInt(ch);
			List<Mat> blurredlist = new ArrayList<Mat>();
			List<Mat> graylist = new ArrayList<Mat>();
			blurredlist.add(0, blurred);
			graylist.add(0, gray0);
			Core.mixChannels(blurredlist, graylist, fromto);
			gray0 = graylist.get(0);
			// System.out.println("Mix Channels Done! : " + gray0.total());
			// try several threshold levels
			int threshold_level = 2;
			for (int l = 0; l < threshold_level; l++) {
				// Use Canny instead of zero threshold level!
				// Canny helps to catch squares with gradient shading
				// System.out.println("Threshold Level: " + l);
				if (l >= 0) {
					Imgproc.Canny(gray0, gray, 20, 30); //

					// Dilate helps to remove potential holes between edge
					// segments
					Imgproc.dilate(gray, gray, Mat.ones(new Size(3, 3), 0));
				} else {
					int thresh = (l + 1) * 255 / threshold_level;
					Imgproc.threshold(gray0, gray, thresh, 255,
							Imgproc.THRESH_TOZERO);
				}
				// System.out.println("Canny (or Thresholding) Done!");
				// System.out.println("Gray Matrix (after)! : " + gray.total());
				// Find contours and store them in a list
				Imgproc.findContours(gray, contours, new Mat(), 1, 2);
				System.out.println("Contours Found!");

				MatOfPoint2f approx = new MatOfPoint2f();
				MatOfPoint2f mMOP2f1 = new MatOfPoint2f();
				MatOfPoint mMOP = new MatOfPoint();
				for (int i = 0; i < contours.size(); i++) {
					contours.get(i).convertTo(mMOP2f1, CvType.CV_32FC2);
					Imgproc.approxPolyDP(mMOP2f1, approx,
							Imgproc.arcLength(mMOP2f1, true) * 0.02, true);
					approx.convertTo(mMOP, CvType.CV_32S);

					if (approx.rows() == 4
							&& Math.abs(Imgproc.contourArea(approx)) > 1000
							&& Imgproc.isContourConvex(mMOP)) {

						System.out.println("Passes Conditions! "
								+ approx.size().toString());
						double maxcosine = 0;
						Point[] list = approx.toArray();
						for (int j = 2; j < 5; j++) {
							double cosine = Math.abs(angle(list[j % 4],
									list[j - 2], list[j - 1]));
							maxcosine = Math.max(maxcosine, cosine);
						}
						if (maxcosine < 0.3) {
							MatOfPoint temp = new MatOfPoint();
							approx.convertTo(temp, CvType.CV_32S);
							squares.add(temp);
						}
					}

				}
				// System.out.println("Squares Added to List! : " +
				// squares.size());
			}
		}

		double maxarea = 0;
		double secmaxarea = 0;
		int maxareaidx = 0;
		for (int idx = 0; idx < squares.size(); ++idx) {
			Mat contour = squares.get(idx);
			double area = Math.abs(Imgproc.contourArea(contour));
			if (area > maxarea) {
				maxarea = area;
				maxareaidx = idx;
			}
		}

		for (int idx = 0; idx < squares.size(); ++idx) {
			Mat contour = squares.get(idx);
			double area = Imgproc.contourArea(contour);
			if (area > secmaxarea && area < maxarea) {
				secmaxarea = area;
			}
		}

		System.out.println("Max Area calculated!" + maxarea);
		System.out.println("Biggest Contour Index" + maxareaidx);
		System.out.println("Second Biggest Area " + secmaxarea);

		if (!(squares.isEmpty())) {
			Mat mask = Mat.zeros(dst.size(), dst.type());
			Scalar s = new Scalar(255, 255, 255);
			Imgproc.drawContours(mask, squares, maxareaidx, s, -1);
			System.out.println("All Contours drawn!");

			Point[] p = squares.get(maxareaidx).toArray();
			System.out.println("Contours of Max Rectangle " + p.length);
			System.out.println("Points of Contour : 1) " + p[0].x + " "
					+ p[0].y);
			System.out.println("Points of Contour : 2) " + p[1].x + " "
					+ p[1].y);
			System.out.println("Points of Contour : 3) " + p[2].x + " "
					+ p[2].y);
			System.out.println("Points of Contour : 4) " + p[3].x + " "
					+ p[3].y);

			Mat canvas = new Mat(image.size(), image.type());
			Scalar s2 = new Scalar(0, 0, 0);
			canvas.setTo(s2);
			image.copyTo(canvas, mask);

			Scalar s1 = new Scalar(255);
			Imgproc.drawContours(canvas, squares, maxareaidx, s1, 1);

			return canvas;
		} else {
			System.out.println("No edges found, showing original image!");
			Mat originalCopy = new Mat();
			image.copyTo(originalCopy);
			return originalCopy;
		}
	}

	/**
	 * return an array of points with the houghLines of the desired Mat canny
	 * performed image
	 * 
	 * @param imgCanny
	 *            image to perform the houghlines detection
	 * @return List of the points array with the houghLines
	 */
	public List<Point[]> getHoughLines(Mat imgCanny) {
		List<Point[]> houghLines = new ArrayList<Point[]>();
		Point[] line;
		Mat lines = new Mat();
		Imgproc.HoughLinesP(imgCanny, lines, 1, Math.PI / 180, 30, 1, 0);
		for (int x = 0; x < lines.cols(); x++) {
			double[] vec = lines.get(0, x);

			if (vec == null)
				break;

			double x1 = vec[0], y1 = vec[1], x2 = vec[2], y2 = vec[3];
			Point start = new Point(x1, y1);
			Point end = new Point(x2, y2);
			line = new Point[2];
			line[0] = start;
			line[1] = end;
			houghLines.add(line);
			// System.out.println("Detected line in " + start.x + ":" + start.y
			// + " <-> " + end.x + ":" + end.y);

		}
		return houghLines;
	}

	public BufferedImage prepareToOcr(Mat originalGray) {
		Mat mGray = new Mat(originalGray.size(), originalGray.type());
		originalGray.copyTo(mGray);
		/**
		 * check the GaussianBlur vs the medianBlur and compare the results
		 */
		// Imgproc.GaussianBlur(mGray, mGray, kernerlSize, 0,
		// Imgproc.BORDER_DEFAULT);
		Imgproc.medianBlur(mGray, mGray, 3);
		/**
		 * this operations improve the text recognition but also the noise,
		 * perform only in cells if the median blur is not enought
		 */
		// int blockDim = (int) Math.min(originalGray.size().height / 4,
		// originalGray.size().width / 4);
		// if (blockDim % 2 != 1)
		// blockDim++; // block has to be odd
		// Imgproc.adaptiveThreshold(mGray, mGray, 255,
		// Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY,
		// blockDim, 0);
		// Core.bitwise_not(mGray, mGray);
		return this.matToBufferedImage(mGray);
	}
}
